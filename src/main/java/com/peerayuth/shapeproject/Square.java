/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peerayuth.shapeproject;

/**
 *
 * @author Ow
 */
public class Square {
    private int a;
    
    public Square(int a) {
        this.a = a;
    }
    
    public int calArea(){
        return a*a;
    }
    
    public void setA(int a) {
        if (a <= 0) {
            System.out.println("Error : Area must more than zero!!");
            return;
        }
        this.a = a;
    }
    
    public double getA() {
        return a;
    }
} 
