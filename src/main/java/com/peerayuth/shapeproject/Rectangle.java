/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peerayuth.shapeproject;

/**
 *
 * @author Ow
 */
public class Rectangle {
    private int l;
    private int w;
    
    
    public Rectangle(int l,int w) {
        this.l = l;
        this.w = w;
    }
    
    public int calArea(){
        return w*l;
    }
    
    public void setlw(int l,int w) {
        if (l <= 0 && w <= 0) {
            System.out.println("Error : Length and Width must more than zero!!");
            return;
        }
        this.l = l;
        this.w = w;
    }
    
    public double getl() {
        return l;
    }
    
    public double getw() {
        return w;
    }
    
}
