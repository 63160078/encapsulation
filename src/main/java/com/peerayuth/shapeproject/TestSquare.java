/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peerayuth.shapeproject;

/**
 *
 * @author Ow
 */
public class TestSquare {
    public static void main(String[] args) {
        Square square1 = new Square(3);
        System.out.println(square1.calArea());
        System.out.println("Area of square(a = " + square1.getA() + ") is "+ square1.calArea());
        square1.setA(2); // square1.a = 2
        System.out.println("Area of square(a = " + square1.getA() + ") is "+ square1.calArea());
        square1.setA(0); // square1.a = 0
        System.out.println("Area of square(a = " + square1.getA() + ") is "+ square1.calArea());
    }
    
}
