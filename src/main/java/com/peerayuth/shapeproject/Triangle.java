/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peerayuth.shapeproject;

/**
 *
 * @author Ow
 */
public class Triangle {
    private double h;
    private double w;
    
    static final double x = 0.5;
    
    public Triangle(double h,double w) {
        this.h = h;
        this.w = w;
    }
    
    public double calArea(){
        return x*h*w;
    }
    
    public void sethw(int h,int w) {
        if (h <= 0 && w <= 0) {
            System.out.println("Error : Height and Width must more than zero!!");
            return;
        }
        this.h = h;
        this.w = w;
    }
    
    public double geth() {
        return h;
    }
    
    public double getw() {
        return w;
    }
    
}
